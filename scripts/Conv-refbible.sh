#!/bin/bash

# pour chaque fichier dans ls *.sfm
for FILE in `ls *.usfm` ; do
#Commence par mettre à la ligne chaque verset
  sed -i 's/Mc /Mk /g' $FILE
  sed -i 's/Lc /Lk /g' $FILE
#Supprime les espaces éventuel en début de ligne
  sed -i 's/Jn /Yo /g' $FILE
 sed -i 's/Ac /Bis /g' $FILE
  sed -i 's/Rm /Rom /g' $FILE
   sed -i 's/Ef /Efz /g' $FILE
    sed -i 's/Ac /Bis /g' $FILE
  sed -i 's/Heb /Ebr /g' $FILE
      sed -i 's/Jcq /Yak /g' $FILE
   sed -i 's/ Pi / Pe /g' $FILE
     sed -i 's/Jud /Yuda /g' $FILE
 sed -i 's/Ap /Bmb /g' $FILE
done
