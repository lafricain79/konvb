#!/bin/bash

#unoconv -f txt *.odt
#unoconv -f txt *.odt
#killall soffice.bin
#Failed to connect to /usr/lib/libreoffice/program/soffice.bin (pid=17913) in 6 seconds.
#Connector : couldn't connect to socket (Success)
#Error: Unable to connect or start own listener. Aborting.
#Pour supprimer les bom des fichiers utf8
find . -type f -exec sed 's/\xEF\xBB\xBF//g' -i.bak {} \; -exec rm {}.bak \;
# pour chaque fichier txt
for FILE in *.txt ; do
##Supprime les lignes vides
sed -i '/./!d' "$FILE"
##Ajoute les codes d'identification
sed -i '1i\\\id \n\\h \n\\toc1 \n\\toc2 \n\\toc3 \n\\mt1' "$FILE"
#Fusionne la ligne 6 avec la suivante (\mt1)
sed -i '6{N; s/\n/ /}' "$FILE"
#Commence par mettre à la ligne chaque verset
  sed -ri 's/\.1/\. 1/g' "$FILE"
  sed -ri 's/([0-9])([A-Z])/\1 \2/g' "$FILE"
 # sed -ri 's/([0-9][0-9])\. /\1 /g' "$FILE"
  #sed -ri 's/([0-9])\. /\1 /g' "$FILE"
  sed -i 's/ [0-9][0-9] /\n&/g' "$FILE"
  sed -i 's/ [0-9] /\n&/g' "$FILE"
#Supprime les espaces éventuel en début de ligne
  sed -i 's/^[ ]*//g' "$FILE"

#Ajout "\v " devant chaque ligne commençant par un puis deux chiffres suivit d'un espace et de texte
  sed -i 's/^[0-9] [a-z]*/\\v &/g' "$FILE"
  sed -i 's/^[0-9][0-9] [a-z]*/\\v &/g' "$FILE"
  sed -i 's/^[0-9][0-9][0-9] [a-z]*/\\v &/g' "$FILE"
  sed -ri 's/»([0-9])/»\n\\v \1/g' "$FILE"
sed -ri 's/”([0-9])/”\n\\v \1/g' "$FILE"
sed -ri 's/([a-z]\.)([0-9])/\1\n\\v \2/g' "$FILE"
sed -ri 's/([a-z]\*\.)([0-9])/\1\n\\v \2/g' "$FILE"
sed -ri 's/([a-z]!)([0-9])/\1\n\\v \2/g' "$FILE"
sed -ri 's/([a-z]\?)([0-9])/\1\n\\v \2/g' "$FILE"
sed -ri 's/([0-9])([a-z][a-z])/\n\\v \1 \2/g' "$FILE"
#ajoute \c devant chaque ligne commencant par un deux ou trois chiffres
#  sed -i 's/^[0-9]+\n/\\c &\n\\p/g' "$FILE"
  sed -i 's/^[0-9]$/\\c &\n\\p/g' "$FILE"
  sed -i 's/^[0-9][0-9]$/\\c &\n\\p/g' "$FILE"
  sed -i 's/^[0-9][0-9][0-9]$/\\c &\n\\p/g' "$FILE"

 # Mettre \s devant les titres
 sed -i 's/^[1-6]\. .*$/\\s2 &\n\\p/g' "$FILE"
 sed -i 's/^[A-Z].*[A-Z]$/\\s2 &\n\\p/g' "$FILE"
 sed -i 's/^[a-f]) .*$/\\s2 &\n\\p/g' "$FILE"
 sed -ri '/^[IVXLCDM]+(\.| |$)/ s/^/\\s /' "$FILE"
 sed -ri 's/^\\s .*$/&\n\\p/g' "$FILE"
  sed -ni '$!N; /[a-z]\n\\c/ s/^[[:upper:]]/\\s2 &/; P; D'  "$FILE"
  sed -ri 's/^[A-Z].*[a-z]$/\\s2 &\n\\p/g' "$FILE"
  sed -ri 's/^[A-Z].*\*$/\\s2 &\n\\p/g' "$FILE"
  
  
 ##Met la balise \r devant les références
 sed -i 's/^(.*)$/\\r &\n\\p/g' "$FILE"

rename 's/txt/usfm/g' "$FILE"
done
##Ce script déplace le titre qui se trouve au-dessus du chapitre, en dessous
$HOME/.bin/titre-chap.sh

