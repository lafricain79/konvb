\id 1PE
\h 1 Petelo
\toc1 Mukanda ya ntete ya santu Petelo kusonikaka
\toc2 1 Petelo
\toc3 1 Pe
\mt Mikanda ya Santu Petelo kusonikaka
\imt MAYITA na 1 Petelo
\is1 Nani sonikaka mukanda yai ?
\ip Nsoniki ke tuba nde : yandi kele ntumwa Petelo. Kele yina ya Yezu kusolaka sambu na kulangidila mameme na yandi (Mk 3,13-19 ye Yo 21,15-17), yina kitukaka temwe ya Kristu ti ya lufutumuku na yandi (Bis 1,22 ye 2,32 ye 10,41). Kansi mutindu mukanda sonamaka mbote-mbote (fr. la qualité de la langue et du style), beto lenda kuyindula nde Petelo, ya vandaka ntete mulobi na dizanga ya Galilea, sonikaka yo ve yandi mosi. Kansi beto ke zaba nde yandi vandaka na kalaki ya kusadisaka yandi na kusonika (fr. un secrétaire). Kele Silvain (tala 5,12), zina na yandi na ki-latin. Na ki-grek zina na yandi kele Silas. Yandi vandaka na kati ya baprofeta ya Yeruzalem (Bis 15,32), mosi na kati ya bayina ya kimvuka ya bakristu (kumosi ti Petelo ye Yakobo) kutindaka na Antioche na kunata mukanda ya ke monika na Bis 15,22-29. Yandi fidisaka Polo na nzietolo na yandi ya zole ya ke yantika na Bis 15,36-40. Awa beto ke mona nde yandi kele ti Petelo.
\is1 Mukanda yai sonamaka sambu na banani ?
\ip Nsoniki ke tuba nde : yandi ke zola kusiamisa lukwikilu ya bakristu (5,12) na baprovince tanu ya Asia (bilumbu yai na Turquie) : yina ya vandaka na ntu-mbanza Efeze, yina ya ba Galate ya Polo kwendaka, ti ba province ya Pont, ya Cappadoce ti ya Bitinia, ya beno ke zaba ve nani mwangaka Evangile kuna. Na kati ya bakristu ya Petelo ke sonikila, bamingi me basika na kati ya bamimpani. Bayina ke twadisa bimvuka yina ya bakristu kele bambuta (fr. des Anciens). Petelo ke zola kupesa kikesa na bakristu ya vandaka kumona mpasi, mu mbandu sambu bamimpani ke bakisa ve luzingu na bo. Yandi ke vutula mpi kikesa na bayina vandaka bampika. Mukanda ke simba beto sambu yo ke basika na ntima ya ke pela na tiya ya lutondo sambu na bampangi-bakristu. Yo ke monika mpi bonso dilongi sambu na bayina me katuka na kubaka mbotika (tala 2,2). Bonso mukanda ya Yakobo kusonikaka, yo ke yibukisa beto malongi ya Yezu, ntete-ntete na zulu ya ngumba. Kansi luswaswanu kele mpi ti mukanda ya Yakobo kusonikaka : awa bo ke yindula maswekamu ya mampasi ti lufwa ya Kristu, kukulumuka na yandi na bwala ya bafwa, lukumu na yandi mfinama na Nzambi Tata, ye Ngisa na yandi ya nkembo, ya bakristu ke kinga. Mbala mingi awa Petelo ke yibukisa bangogo ya Ngw.ya nkulu (na mutindu ya bo balulaka yo na ki-grek) (fr. La Septante : LXX).
\is1 Ntangu ya bo sonikaka mukanda
\ip Na bamvula ya kulutaka, bo yindulaka nde : Petelo sonikaka yo na bamvu 62-63, ntangu mingi ve na ntwala ya lufwa na yandi. Kansi keti mbangika ya bakristu monikaka ntete na bamvula yina ? Na mvu 64, Sezari Néron fundaka bakristu nde : bo bantu tulaka tiya na kuyoka mbanza Roma. Kansi bo ke yindula nde yandi mosi salaka yo, sambu yandi vandaka na nzala ya kutunga diaka mbanza na mutindu ya kuluta kitoko. Kansi mbangika yina bwilaka kaka kimvuka ya bakristu ya Roma. Leta pesaka ve nsiku ya kubangika bakristu na bambanza yankaka ya Kimfumu na yandi. Na mambu ya ke tadila baprovince ya Asia (bilumbu yai Turquie), beto kele na mukanda ya Pline, gouverneur ya Bitinia, kusonikaka na Sezari Trajan na mvu 112 na kuzaba kana yandi fwete kubangika diaka bakristu. Mukanda na yandi ke yibukisa mbangika ya Sezari Domitien kutumaka kiteso ya bamvu 20 ya ku­yitaka : na bamvu 90-96, ya lenda vanda mpi ntangu ya Yoani kusonikaka Bambona-meso na yandi. Mpila yina, bantu mingi ya ma­zaya ke yindula nde : 1 Petelo sonamaka na bamvula yina 90-96. Kansi bantu yankaka ya mazaya ke tuba nde : mukanda yai ke lomba na kuzitisa bamfumu ya Leta, ya ke sala kisalu ya mbote (2,14). Ye mukanda yai ke tuba ve nde Leta ke bangika bakristu, kansi yo ke zabisa mampasi ya ke kondaka ve na luzingu ya bakristu na ntoto yai. Yo yina, bantu yina ya mazaya ke tuba nde : kana Petelo kusonikaka ve mukanda yai yandi mosi, ziku Silvain sonikaka yo ntangu mingi ve na nima ya lufwa ya Petelo : na bamvu 65 - 75, na Roma ya yandi ke binga Babilone.
\is1 Mukanda sambu na beto ntangu yai
\ip Nsoniki ke sonika sambu na bantu ya ntangu na yandi. Mpila yina beto ke yituka na kumona bandongisila yankaka ya yandi ke pesa na bankento ti na bampika. Luvunu ve, nsoniki ke zola ve kubalula mutindu bantu vandaka kuzinga, kansi yandi ke lomba na kuzitisa konso muntu. Yandi ke lomba mpi na kuzinga luzingu ya kimpwanza (2,16) kansi yo kuvanda ve mutindu ya kubumbisa masumu. Kuluta mbote bakristu kumona mpasi, kansi bo kusala mbi ve, bo kuzinga ve luzingu ya nku (2,18-20). Bakristu vandaka bantu mingi ve na kati ya bamimpani ya ntangu yina, kansi luzingu na bo fwete kutemuna bamimpani, sambu bo kubakisa kimvwama ya lukwikilu na beto (2,12.15 ye 3,1). Bakristu fwete kumonikisa kivuvu na bo na mawete ti na luzitu. (3,13-16). Bakristu kele kumosi « nzo ya Nzambi » (2,4-5) ; mpila yina bantu ya bamfumu ti bamvwama ke vwezaka, bo lenda kumona na kati ya Dibundu kimvuka ya ke yamba bo, ke natila bo ngemba, ti ya bo fwana kutula ntima.
\is Mutindi mambu ke landana na mukanda yai
\tr \tc1 Petelo ke pesa mbote : \tc2 1,1-2
\ip
\tr \tc1 Nsoniki ke pesa matondo na Nzambi,na mutindu ya bayuda (tala mpi Efz 1,3-14) : \tc2 1,3-12
\li1 Nkembo ya Tata ya ke binga beto na luzingu ya mpa (vv.3-5)
\li1 Kiese na beno na kati ya mampasi (vv.6-7)
\li1 Kiese sambu na Kristu ya me gulusa beno (vv.8-9)
\li1 Mpeve Santu temunaka baprofeta ; bo sosaka na kubakisa na ntwala mambu ya me lunga ntangu yai ; yo ke kwendila ti lunungu ya Kristu (vv.10-12).
\ip
\tr \tc1 1. Kuzingila busantu na kati ya Dibundu : \tc2 1, 13 - 2, 10
\li1 a) Kivuvu na beto ke lomba luzingu ya santu (1,13-21) : sambu Mwana-Dimeme ya Nzambi me gulusa beto (vv.17-21)
\li1 b) Kuzinga luzingu ya bana ya Nzambi (1,22-2, 3) : na kati ya lutondo (1,22-25), bonso bantu ya mpa (2,1-3)
\li1 c) Kuvanda kikanda ya Nzambi (2,4-10)
\li2 beto kele matadi ya Mpeve ke tungila Nzo ya Kristu kele fu­ndasio
\li2 yo yina, beto mwanga bisalu ya nene ya Nzambi ya me temuna beto.
\ip
\tr \tc1 2. Luzingu ya bakristu na ntoto yai : \tc2 2,11–3,12
\li1 a) na kati ya bamimpani (vv.11-12)
\li1 b) na meso ya bamfumu ya Leta (vv.13-17)
\li1 c) bansadi-bakristu na meso ya bamfumu na bo (vv.18-25) * mbandu ya Kristu kubenda beno (vv.21-25)
\li1 d) na kati ya makwela (3,1-7)
\li1 e) Kuvanda bantu ya lutondo, ya mawete (3,8-12)
\ip
\tr \tc1 3. Luzingu na beno kuzabisa nde beto me guluka : \tc2 3,13–4,11
\li1 a) beno zaba kutendula kivuvu na beno (3,13-17)
\li1 b) Nsangu ya mbote me kuma ata na bwala ya bafwa (3,18-22)
\li1 c) Yo yina, beno kusumuka diaka ve (4,12-6)
\li1 d) Beno sadisana beno na beno (4,7-11)
\ip
\tr \tc1 4. Beno bantu ya ke kinga Ngisa ya Kristu : \tc2 4, 12-5, 12
\li1 a) Beno mona kiese na kati ya mampasi (4,12-19)
\li1 b) Beno Bambuta ya Dibundu, langidila mameme (5,1-4)
\li1 c) Beno baleke (5,5a)
\li1 d) Beno yonso kuditula na maboko ya Nzambi (5,5b-11)
\ip
\tr \tc1 5. Na kumanisa mukanda : \tc2 5,12-14
\li1 a) Silvain me sadisa mono (5,12)
\li1 b) Mbote ya nsuka (5,13-14)
\c 1
\s1 Mukanda ya ntete ya santu Petelo kusonikaka
\p
\v 1 Mono Petelo, ntumwa ya Yezu Kristu, mono ke sonikila beno bakristu ya kele bonso banzenza na ntoto yai, beno ya me mwangana na kati ya bamimpani ya ba provi­nce ya Pont, ya ntoto ya ba Galate, ya Cappadoce, ya Asia ti ya Bitinia.
\v 2 Beno bantu ya Nzambi Tata me sola, mutindu yandi yindulaka na ntwala, beno bantu ya yandi ke santisa na Mpeve santu, sambu beno kulemfuka na Yezu Kristu, ya ke gulusa beno na menga na yandi ya me tiamuka, bika Nzambi kupesa beno mambote ya Dienga ti Ngemba na mutindu ya kuluta kiteso !
\s1 Matondo na Tata sambu na lugulusu ; ye kivuvu ya Kristu ke natila beto
\p
\v 3 Lukumu kuvanda na Nzambi, Tata ya Mfumu na beto Yezu Kristu, sambu yandi me songa beto mawa mingi. Ntangu yandi telemisaka Yezu Kristu na kati ya bafwa, yandi me buta beto na mutindu ya mpa, sambu na kivuvu ya ke zingisa beto.
\v 4 Mpamba ve : yandi ta kabila beto kimvwama ya ke bebaka ve, ya ke kumaka mvindu ve, ya ke polaka ve. Yandi ke bumbila beno yo na zulu,
\v 5 beno bantu ya Nzambi ke tanina na ngolo na yandi na kati ya lukwikilu, sambu na lugulusu ya ta monika na ntangu ya nsuka ya me finama.
\v 6 Yo ke sepelisa beno, ata beno kele na mawa sambu na ma­mpasi ya mutindu na mutindu ya mwa-ntangu ya bilu­mbu yai.
\v 7 Mampasi yina kele sambu na kumeka lukwikilu na beno, ya ke luta wolo na mbalu. Bo ke mekaka wolo na tiya, ata yo kele kima ya ke bebaka. Mutindu yina, Nzambi ke tala kana lukwikilu na beno kele ya kieleka, sambu yo kupesa lukumu ti nkembo na Nzambi ntangu Yezu Kristu ta kwisa kumonika.
\v 8 Beno ke tonda yandi, ata beno monaka yandi ve na meso. Beno ke kwikila na yandi, ata beno me mona yandi ntete ve. Yo yina, beno ke yangalala na kiese ya kukonda ndilu, kiese ya ke kwendila ti nkembo,
\v 9 sambu beno ta nunga, beno ta zwa yina ya lukwikilu ke buta : lugulusu na beno.
\v 10 Lugulusu yina, baprofeta yindulaka ngolo sambu na kubakisa yo, ntangu bo zabisaka na ntwala mambote ya dienga ya Nzambi kuyidikilaka beno.
\v 11 Bo sosaka na kubakisa mambu ya Mpeve ya Kristu songaka bo na ntima, ntangu yandi zabisaka bo na ntwala mampasi ya Mesiya fwete kumona ti nkembo ya fwete kulanda yo. Bo sosaka na kuzaba ntangu na yo ye mutindu yo fwete kubwa.
\v 12 Pana Nzambi songaka bo nde : malongi ya yandi kutulaka na munoko na bo, yo vandaka sambu na ntangu na bo ve, kansi sambu na beno. Yo yai malongi ya bayina me mwanga Nsangu ya mbote me pesa beno bilumbu yai na nsadisa ya Mpeve Santu ya Nzambi kutindilaka bo. Yo yai malongi ya banzio kele na nzala ya kubakisa.
\s Dilongi ya ntete
\s1 Kuzingila busantu na kati ya Dibundu
\s2 Kivuvu ya ke lomba luzingu ya busantu
\p
\v 13 Yo yina, beno yilama sambu na kusadila Nzambi. Beno kangula meso, bonso bantu me lawuka malafu ve. Beno tula kivuvu na beno ya mvimba na mambote ya dienga, ya Nzambi ta pesa beno ntangu Yezu Kristu ta monika.
\v 14 Bonso bana, beno lemfuka na Nzambi. Beno landa diaka ve banzala ya kimuntu ya beno vandaka kuzingila na bilumbu ya ntama, ntangu beno vandaka me temuka ntete ve.
\v 15 Kansi mutindu Nzambi ya me binga beno kele santu-santu, beno mpi, na luzingu na beno ya mvimba, beno kukituka bantu ya santu,
\v 16 sambu bo sonikaka na mikanda ya Nzambi nde : \qt Beno vanda bantu ya santu, sambu Mono kele santu-santu.\qt*\f + \ft Yo ke monika na Lev 11,44-45 ye 19,2.\f*
\s2 Beno me guluka na menga ya Kristu, mwana-dimeme ya Nzambi
\p
\v 17 Bampangi, na bisambu na beno, beno ke bingaka « Tata » yina ke talaka bantu ve na luse na kuzaba kana bo kele banani, kansi yina ke sambisa bo, konso muntu na mutindu ya bisalu na yandi. Yo yina, beno songa yandi luzitu ya nene na bilumbu yai ya beno ke luta na ntoto yai.
\v 18 Beno me zaba mutindu Nzambi gulusaka beno, ntangu yandi katulaka beno na luzingu ya mpa­mba ya bambuta na beno kubikisilaka beno : yandi gulusaka beno ve na mbongo to na wolo, bima ya ke bebaka,
\v 19 kansi na menga ya mbalu mingi na meso na yandi : bonso menga ya mwana-dimeme ya kukonda kifu to ditona, menga ya Kristu,
\v 20 ya Nzambi yitaka kusola na ntwala ya kuganga zulu ti ntoto, yina me monika sambu na beno na ntangu yai ya kele ntangu ya nsuka.
\v 21 Na nzila na yandi Yezu, beno ke kwikila na Nzambi ya me tele­misa yandi na kati ya bafwa na kupesa yandi lukumu. Mpila yina beno ke tula lukwikilu ti kivuvu na beno na Nzambi.
\s1 Kuzinga luzingu ya bana ya Nzambi
\s2 A. Na kati ya lutondo
\p
\v 22 Ntangu beno me lemfuka na makieleka, beno me gedidisa ntima na beno, sambu na kuzingila pwelele lutondo ya kimpangi. Beno tondana ngolo beno na beno, na ntima ya mvimba.
\v 23 Sambu beno me butuka na luzingu ya mpa, ya ke basika ve na batata ya ntoto yai, ya ke fwaka, kansi luzingu ya ke basika na Ndinga ya Nzambi, ya kele bonso nkeni ya ke fwaka ve, sambu yo ke zinga, yo ke mana ve.
\v 24 Sambu mikanda ya santu ke tuba nde : \qt Bantu yonso kele bonso matiti. Lukumu na bo kele bonso yina ya bintuntu. Matiti ke yumaka, bintuntu ke bebaka.\qt*
\v 25 \qt Kansi ndinga ya Mfumu Nzambi ke zinga mvula na mvula.\qt* Ye Ndinga yina kele Nsangu ya mbote ya bo kuzabisaka beno.\f + \ft Na vv.24-25a, nsoniki ke yibukisa beto Iza 40,6-8. Ndinga ya Nzambi kele bonso nkeni ya ke zola kuyela na bantima na beto : na v.23 ya ke kwendila ti Mt 13,3-9. Ye nsoniki ke yibuka (na v.25b) ndinga ya Iza 40,9 ya ke tadila bayina ke mwanga Nsa­ngu ya mbote. Mpila yina awa, Ndi­nga ya Nzambi kele bonso nkeni ya bantumwa ti bilandi na bo me kuna na ntima ya bakristu.\f*
\c 2
\s2 B. Bonso bantu ya mpa
\p
\v 1 Yo yina, beno bikisa mambi yonso ti mayele yonso ya luvunu. Beno kukusa bantu diaka ve. Beno bikisa ki­mpala ti songi-songi yonso.
\v 2 Beno vanda bonso bana ya me katuka kubutuka. Ndinga ya Nzambi kuvandila beno bo­nso mabele ya bo ke zola kunwa. Beno vanda na nzala na yo sambu yo kuyedisa beno tii kuna ya beno ta guluka.
\v 3 Mpamba ve : \qt Beno me mona kiese na mambote ya Mfumu, bonso na madia ya ntomo.\f + \ft Ndinga yai me katuka na Nkb 33 (h 34,9) ya ke tadila mu­tindu Nzambi ke tanina ba­ntu ya masonga. Awa yo ke tadila Ndi­nga ya Nzambi ya ke disa bayina ke ndima na kuyela na kimukristu.\f*\qt*
\s1 Kuvanda Tempelo ya Nzambi ye kikanda ya kinganga-Nzambi
\p
\v 4 Bampangi, beno finama na Mfumu Yezu ; yandi kele Ditadi ya ke zinga kibeni, yina ya bantu kubuyaka, kansi ya Nzambi me sola, sambu yo kele ditadi ya mbalu na meso na yandi.
\v 5 Mpila yina Nzambi ta sadila beno bonso matadi ya moyo, na kutunga nzo ya Mpeve Santu ta zingila, na mpila nde beno ku­kituka kimvuka ya santu ya kinganga-Nzambi, sambu na kutambika na Nzambi bimenga ya Mpeve Santu ta natila ya­ndi, bimenga ya Nzambi ta ndima na kiese sambu na Yezu Kri­stu.
\v 6 Yo yina, bo ke tanga na Mikanda ya santu nde : \qt Tala ! Mono ke tula na ngumba Sion ditadi ya ta simbisa nzo. Yo kele ditadi ya mono me sola, ditadi ya mbalu na meso na mono. Yina ke tula ntima na yandi na yo, yandi ta mona nsoni ve.\f + \ft Ndinga yai ke basika na Iza 28,16 mutindu bo balulaka yo na kigrek (LXX). Ditadi yina kele Kristu, fundasio ya Tempelo ya mpa, ya ke kwendila ti lusilu ya Nza­­mbi silaka (mu mbandu na munoko ya profeta Ezekiel). Kristu ya bamfumu ya bayuda kubuyaka, me futumuka : yandi kele Ditadi ya ke zinga, yina ke vukisa beto sa­mbu beto kuvanda, kumosi ti yandi, Tempelo ya mpa, mpila mosi kimvuka ya ke kembila Tata, kwa beto ke tambikila ya­ndi luzingu na beto ye lu­zingu ya bantu ya ntoto mvimba. Na v.7, bo ke yibukisa Nkb 117 (h 118,22) ya Yezu kubakaka sa­mbu na kuzabisa na ntwala lufwa ti lufutumuku na yandi (tala Mt 21,42). Na v.8a, bo ke yibukisa Iza 8,14 na ngindu nde na ntwala ya Kristu, mpila kele ve beto kusola ve : beto ta vanda bantu na yandi, to bambeni na yandi.\f*\qt*
\v 7 Yo yina, lukumu na beno, bantu ya ke kwikila. Kansi mawa na bayina kele na lukwikilu ve, sambu \qt Ditadi ya ba-maçon kubuyaka, yo me kituka Ditadi ya ke simbisa nzo.\qt*
\v 8 Mikanda ya santu ke tuba diaka nde : \qt Ditadi yina me kumina bo sakuba, ditadi ya ke bwisa bo.\qt* Bantu yina ke bula dibaku, sambu bo ke buya na kukwikila na Ndinga ya Nzambi. Yai mambu ya fwetele kwisila bo.
\v 9 Kansi beno, \qt beno kele kikanda ya Nzambi me sola, bantu ya kintotila, kimvuka ya kinganga-Nzambi, bantu ya Nzambi me santisa, kikanda ya yandi me kukibakila, na mpila nde beno kuzabisa bisalu ya nene\qt* ya Nzambi ya kubingaka beno na kukatuka na mpimpa sambu beno kuvanda na nsemo na yandi ya ke ngenga.\f + \ft Na v.9 bo ke yibukisa beto Kub 19,5-6 (Nzambi ke sila na bantu ya Israël nde bo ta vanda bantu na yandi, kana bo kundima Ngwakana ti bansiku na yo) ya nsoniki ke vukisa ti Iza 43 , 20-21 (bantu ya Nzambi me sola, bo ta kembila bisalu ya Ngwakana na yandi ya mpa). Na v.10, bo ke yibukisa Oz 1,6.9 ye 2,1.25 ya ke tadila mazina ya bana ya Ozea (bantu na mono ve, sambu Mfumu Nzambi ke zola bo diaka ve ; bantu na mono, ntangu Nza­mbi ke ndima bo diaka. Kiteso mosi na mambu ya ke tadila mawa ya Nzambi ke wila bo).\f*
\v 10 Na bamvula ya ntama, beno vandaka \qt bantu na yandi ve,\qt* kansi ntangu yai beno kele kikanda ya Nzambi. Na ntangu yina, Nzambi \qt wilaka beno mawa ve.\qt* Kansi ntangu yai, \qt yandi me wila beno mawa.\qt*
\s1 Dilongi ya zole : Luzingu ya bakristu na ntoto yai. 
\s1 Na kati ya bamimpani
\p
\v 11 Bampangi ya kutondama, awa na nsi-ntoto, beno kele ba­nze­nza. Yo yina, mono ke bondila beno na kutina banzala ya kimuntu, sambu yo ke nwanisa ntima na beno.
\v 12 Na kati ya bamimpani, beno zinga luzingu ya mbote na mpila nde : ata bo kutonga beno bonso bantu ya mbi, kansi bisa­lu na beno ya mbote ta kukangula meso na bo, ibuna bo ta pesa lukumu na Nzambi, na kilumbu ya yandi ta kwisa kuzenga nkanu.
\s1 Na meso ya bamfumu ya Leta
\p
\v 13 Beno lemfuka na bamfumu ya ntoto yai, sambu na luzolo ya Mfumu Nzambi. Yo ke tadila ata ntotila, sambu yandi muntu kele na ntwala,
\v 14 ata ba gouverneur ya yandi me tula na kupesa ndola na bantu ya nku ti kukembila bantu ya ke zingaka mbote.
\v 15 Mpamba ve : luzolo ya Nzambi, yo yai : beno kusala mambote na kukangisa munoko ya bazoba ya ke zaba ve mambu ya bo ke tuba.
\v 16 Beno zinga bonso bantu ya kimpwanza. Kansi beno kubalula yo ve lele ya kufika mambi na beno. Beno vanda bantu ya ke sadila Nzambi.\f + \ft Na v.16, nsoniki ke songa na kuvanda ve bampika ya ma­mbi ti ya Satana, kansi na kusadila Nzambi (fr. telle est la vraie liberté) ; mpila yina beto ta mona kiese. Na v.17, bo ke yibukisa be­to Bng 24,21 kansi awa bo ke yika nde « Beno tonda bantu yo­nso » ; ye luswaswanu ke monika na kati ya mambu yai zole : kuzitisa ntotila ti kupesa luzitu ya kuluta nene na Nzambi.\f*
\v 17 Beno zitisa bantu yonso ; beno tonda bampangi na beno ; beno songa luzitu ya nene na Nzambi ; beno zitisa ntotila.
\s1 Bansadi na meso ya bamfumu na bo
\p
\v 18 Beno bansadi, beno lemfuka na bamfumu na beno na lu­zitu yonso. Yo ke tadila ve kaka bayina ya ntima ya mbote ti ya mawete, kansi mpi bayina kele makasi.
\v 19 Sambu ku­ndima mpasi ya bo ke pesa beto ya kukonda nsoki, kundima yo sambu beto ke yindula Nzambi, yo kele dikabu ya dienga na yandi.
\v 20 Mpamba ve : kana bo ke bula beno sambu beno mosi me sala mbi, ibuna beno ke tuba kima ve, pana nki lukumu beno me baka ? Kansi kana bo ke pesa beno mpasi, ata bisalu na beno vandaka ya mbote, ibuna beno ke tuba kima ve, pana yo kele dienga na meso ya Nzambi.
\s1 Mbandu ya Kristu
\p
\v 21 Yo yai lubokilu na beno. Sambu Kristu mpi monaka mpasi, ye yandi ndimaka yo sambu na beno. Yandi me bikisa beno mbandu, sambu beno kulanda makulu na yandi.
\q1
\v 22 Yandi muntu ya \qt kusumukaka ve.\qt*
\q2 \qt Na munoko na yandi, bo waka luvunu ve.\f + \ft Ba vv.22-25 ke kwendila ti ba­ngi­ndu ya Iza 53, 4.5.6.9 : Yezu ke lu­ngisa na mutindu ya mpa ti ya kimakulu ngindu ya ke tadila Nsadi ya ke mona mpasi sambu na bantu ya Israël (kikanda na yandi). — Na v.24a bo ke tuba nde Yezu fwaka na nti, disongidila nde na kulunsi. Bo ke baka ngogo yai nti sambu na kuyibukisa Bns 21,22-23 (tala mpi Ga 3,13). — Na v.25 ngindu ya Ngungudi ke kwendila ti 5, 4 ye Yo 10 ti Lk 15,3-7 ti Mt 26,31 : Yezu kele Ngungudi na beto ya kieleka, yina ke tanina beto, yina ke songa beto nzila ya zulu.\f*\qt*
\q1
\v 23 Yandi muntu ya bo vandaka kufinga,
\q2 kansi yandi vutulaka mafingu ve,
\q2 na kati ya mampasi, yandi dasukaka ve,
\q2 yandi bikisaka makambu na yandi na Zuzi ya masonga.
\q1
\v 24 Yandi muntu \qt nataka masumu na beto\qt*
\q2 na nitu na yandi, na zulu ya kulunsi,
\q2 sambu luzingu na beto ya masumu kufwa
\q2 ye beto kuzinga na masonga ya Nzambi.
\q2 Yandi muntu \qt belulaka beno na bamputa na yandi.\qt*
\p
\v 25 Sambu beno vandaka \qt bonso mameme ya ke yungana,\qt* kansi ntangu yai beno me vutuka na Ngungudi ya ke talaka beno, yina ke taninaka ntima na beno.
\c 3
\s1 Na kati ya makwela
\s2 A. Mambu ya bankento
\p
\v 1 Kiteso mosi, beno bankento, beno lemfuka na babakala na beno, na mpila nde : ata bantu yankaka na kati na bo ke buya na kukwikila Ndinga ya Nzambi, beno balula ntima na bo : na ndinga ve kansi na mutindu ya beno bankento ke zinga,
\v 2 ntangu bo ta mona luzingu na beno ya gedila ti ya luzitu.
\v 3 Kitoko na beno kuvanda ve ya nganda, bonso nsuki ya bo ke tunga, bima ya wolo ti bilele ya kitoko ya bo ke lwata.
\v 4 Kansi beno sosa kitoko ya bikalulu ya ke bumbana na ntima : moyo ya pima, ya mawete. Yo yai kitoko ya ke bebaka ve, mambu ya mbalu na meso ya Nzambi.
\v 5 Yo yai kitoko ya bankento ya santu ya ntangu ya nkulu. Bo vandaka kulemfuka na babakala na bo, sambu kivuvu na bo vandaka na Nzambi.
\v 6 Beno tala mbandu ya Sara : yandi lemfukaka na Abraham ya yandi vandaka kubinga Mfumu na yandi. Beno ke kituka bana na yandi ya bankento nta­ngu beno ke sala mambote, ya kukonda kutita ata kima mosi.
\s2 B. Babakala\f + \ft Na 3,7 : Kisina ya luzitu ya bakala fwete kusonga na nkento na ya­ndi, yo yai : bo zole ke zwa lu­bo­ki­lu kiteso mosi ; Nzambi ke bi­nga bo zole na kiese ya kuko­nda nsuka.\f*
\p
\v 7 Kiteso mosi, beno babakala, beno zinga luzingu ya ma­kwela na mutindu ya mbote, bonso bantu ke zaba nde ba­nkento kele ngolo mingi ve bonso beno. Yo yina beno pesa bo luzitu, sambu Nzambi ta kabila bo, nzila mosi ti beno, kimvwama ya dienga, nde Luzingu ya kukonda nsuka. Kana beno kuzitisa bo mutindu yina, ata kima mosi ve ta bebisa bisambu na beno. Bantu ya lutondo ti ya mawete
\v 8 Bampangi, beno yonso, beno vanda na ngwakana, bantu ya ngindu mosi. Beno tondana bonso bampangi, beno monina nkweno mawa, beno kukikulumusa na ntwala ya bantu yankaka.
\v 9 Beno vutula ve mbi na mbi to mafingu na mafingu. Kansi beno zodila bantu yankaka lusambulu ya Nzambi, sambu yo yai lubokilu na beno. Mpila yina, Nzambi ta kabila beno ki­mvwama na yandi, nde lusambulu ya kukonda nsuka.
\v 10 Mpamba ve :
\q2 \qt Yina ke zola luzingu ya mbote\qt*
\q2 \qt ye kumona bilumbu ya kiese,\qt*
\q2 \qt bika ludimi na yandi kutina mbi,\qt*
\q2 \qt munoko na yandi kutuba ve ndinga ya luvunu.\qt*
\q1
\v 11 \qt Yandi kubikisa mambu ya mbi,\qt*
\q2 \qt yandi kusala mambote.\qt*
\q2 \qt Yandi kusosa ngemba,\qt*
\q2 \qt ye kufula na kulanda yo kaka.\qt*
\q1
\v 12 Sambu \qt Mfumu Nzambi ke tula meso na yandi\qt*
\q2 \qt na bantu ya masonga.\qt*
\q2 \qt Yandi ke kangula makutu\qt*
\q2 \qt na yandi na bisambu na bo.\qt*
\q2 \qt Kansi luse na yandi ke baluka,\qt*
\q2 \qt nakunwanisa bayina ke salaka mbi.\f + \ft Ba vv.10-12 ke yibukisa Nkb 33 (h 34,13-17).\f*\qt*
\s1 Dilongi ya tatu
\s2 Luzingu na beno kuzabisa lugulusu ya kele kisina ya kivuvu na beto
\s1 Beno zaba kutendula kivuvu na beno
\p
\v 13 Nani ta kota beno nsoki kana beno kudipesa na kusala kaka mambote ?
\v 14 Kansi kana beno ke niokama sambu na luzingu ya masonga, beno monina yo kiese. Mikanda ya santu ke tuba nde : \qt Beno kutita bantu yina ve ; bo kutula ntima na beno ve na zulu-zulu.\f + \ft Na vv.14b-15a, nsoniki ke ta­ngu­lula Iza 8,12-13 ya ke kwendila ti 8,14 ya Petelo kuyibukisaka na 2,8a. Profeta ke lomba na ku­zitisa Nzambi. Yandi ke songa na bantu ya lukwikilu nde : « Beno ku­tita ve bayina ke landaka bangi­ndu ya ntoto yai, bayina ke lomba na kuwakana ti bantotila ya ke kwi­sa kunwanisa Yeruzalem ti Kimfu­mu ya Yuda. Beno kukangama na Nza­mbi, yina kele ya santu-santu ; be­no zitisa yandi na luzingu ya sa­ntu. Kansi bantu ya Yeruzalem ya ke kangama ve na yandi, bayina me balula ntima ve na kuvutuka na Mfu­mu na bo, bo ta bwa na mu­tambu, bo ta bula sakuba na Di­ta­di. » Mutambu kele nki ? Ditadi ke­­le nki ? Mbala yankaka yonso zo­le kele na mbala mosi Nzambi ya santu ye Ki­mfu­mu ya ke kwisa kunwana na kubwisa Yeruzalem. Sambu Nzambi ta lola bayina me kangama ve na yandi. Ndola na bo ta vanda yai : kuzinga na makulu ya bamfumu ya ta wakana ti bo kaka na kuniokula bo (Dieu est seul maître des événements ; il faut lui faire totale confiance et vivre dans son Alliance, sans se compromettre avec des rois ambitieux). — Awa bakristu fwete kumona boma ve ya bayina ke pesa bo mpasi. Bo fwete kuzinga luzingu ya santu ya ta kembila Mfumu Yezu. Yo yai mvutu ya kuluta mbote ya bo ta pesa na bayina ke bangika bo.\f*\qt*
\v 15 Kansi \qt na luzingu ya santu,\qt* beno zitisa na bantima na beno Kristu ya kele Mfumu. Ntangu bo ke yuvula beno sambu na nki beno kele na kivuvu, beno vanda ntangu yonso ya kuyilama na kutendula yo.
\v 16 Kansi beno tendula yo na mawete ti na luzitu. Ntima na beno kuyindula ti masonga, na mpila nde bayina ke tonga luzingu na beno ya kitoko ya Kristu ke zingisa beno, beno kubedisa bo na mambu yo yina kibeni ya bo ke kusila beno.
\v 17 Mpamba ve : kuluta mbote, kana yo kele luzolo ya Nzambi, beno kumona mpasi sambu na mambote ya beno ke sala, kansi yo kuvanda ve sambu beno me sala mbi.
\s1 Nsangu ya lugulusu na bwala ya bafwa\f + \ft Yo kele mpasi na kubakisa vv.18-22. Kansi ngindu yantete ya ndambu yai kele nde bakristu fwete kubalula ntima ye kubuya kimakulu banzambi ya luvunu ya bami­mpani vandaka kundima. Bakristu fwete kukangama na Yezu Kristu, ya me nunga kimakulu ngolo ya Lufwa ti ya kimfumu ya mambi. Yo yina, na luyantiku ti na nsuka (v.18 ye v.22) nsoniki ke yibukisa beto Lutelu-lukwikilu ya ntangu na yandi : Kristu fwaka sambu na ku­gulusa beto ; yandi me kuma na nke­mbo ya zulu, na diboko ya ki­bakala ya Nzambi Tata. Ye na zulu, yandi ke yala bampeve mutindu yonso. Ngindu ya kuyala na zulu ke kwendila ti Nkb 109 (h 110). Ibuna Petelo ke yindula awa na mutindu ya bambuta ya bayuda. Bo vandaka kutuba nde : na nima ya lu­fwa, mioyo ya bantu ke kwe­­nda ku­zinga na kati ya ma­bu­lu na nsi ya ntoto. Kuna bo ke zinga luzingu ya ke kwendila ti mutindu ya bo zi­ngaka na nto­to yai : bayina ku­zi­ngaka mbote, bo ke zinga mwa-mbote (bo ke kinga lufutumuku) ; bayina kuzi­ngaka na mutindu ya mbi, bo ke mona mpasi (tala ma­mbu mvwama ti ya Lazare na Lk 16,19-26). Bo yonso ke kinga ntangu ya lufutumuku sambu na kukota na luzingu ya kiese ya ku­konda nsuka (yai sambu na bantu ya masonga), to sambu na kuzwa ndola ya kukonda nsuka (ya ke tadila bantu ya mbi). Tala Yo 5,29. Na mukanda mosi, bo vandaka kutuba nde Henoch kwendaka na bwala ya bafwa (na « Kalu­nga ») na kuzabisa banzio ya Nza­mbi kulaka na zulu nde : Nzambi me zenga bo nka­nu ya kukonda nsuka. Na ma­mbu ya ke tadila bantu, na meso ya bambuta ya Israël, bansumuki ya kuluta nene vandaka bayina ya Sodome ti ya Gomorrhe (tala 2 P 2,6) kumosi ti bantu ya mbi ya ntangu ya Noé. Yo yina, awa bo ke yibuka ntangu ya mvula ya ngolo-ngolo ya Nzambi tindaka sambu na kulola bansumuki yina. Na ngindu ya nsoniki, Kristu kwendaka kuzabisa bo, na bwala ya bafwa, nde bo fwete kubaka lukanu : kubuya to kundima Lugulusu. Ibuna ntangu ya Noé ke yibukisa na nsoniki mambu ya maswa. Ye maswa kele awa bonso kidimbu ya ke zabisa na ntwala mbotika.\f*
\p
\v 18 Sambu Kristu yandi kibeni monaka mpasi ya lufwa, mbala mosi imene, sambu na masumu ya bantu. Yandi muntu ya masonga, yandi fwaka sambu na bantu ya nsoki, na mpila nde yandi kunata beno na Nzambi. Bo fwaka yandi na nitu na ya­ndi, kansi Mpeve Santu vutudilaka yandi luzingu.
\v 19 Ibuna yandi kwendaka kumwanga Nsangu ya mbote na bayina vanda­ka na boloko ya lufwa.
\v 20 Kele bayina kubuyaka na kukwi­kila na bamvula ya nkulu, ntangu Nzambi sukininaka na kutinda ndola : ntangu Noe tungaka maswa ya kugulusaka bantu na kati ya masa, bantu mingi ve : nana mpamba.
\v 21 Mambu yina vandaka kidimbu ya kuzabisaka na ntwala mbotika ya ke gulusa beno ntangu yai. Mbotika ke katulaka ve mvindu ya ni­tu, kansi yo kele lusilu ya kwenda na Nzambi na ntima ya ma­songa. Yo ke gulusa beno sambu na lufutumuku ya Yezu Kristu.
\v 22 Yandi muntu me tomboka na zulu, me vanda na diboko ya kibakala ya Nzambi, ya me tula na makulu na yandi banzio ti bambuta-banzio ti bampeve ya vandaka kuyala ntoto.
\c 4
\s1 Yo yina, beno kusumuka diaka ve ; Beno vanda batemwe ya Nsangu ya mbote
\p
\v 1 Kristu monaka mpasi na nitu na yandi. Yo yina, beno mpi, beno tula ntima na mambu yai : mukristu ya me mona mpasi na nitu na yandi, yandi me kabana ti Disumu.
\v 2 Na ba­mvula ya beno ke zinga diaka na ntoto yai, beno landa diaka ve banzala ya kimuntu, kansi luzolo ya Nzambi.
\v 3 Sambu beno lutisaka ntangu mingi na kuzinga na mutindu ya bamimpani. Beno vandaka kuzingila mansoni ti nzala ya mambi, kunwa mpila ve ti kulawuka malafu, kudia mbiki ye kusala mambu ya nsoni na bankinsi ya bamimpani vandaka kukembila banzambi na bo ya luvunu.\f + \ft Awa bo ke tanga ndonga ya mazina ya mambu ya mbi, bonso na Rom 1,29-31. Nsoniki ke yindula ntete mutindu bantu vandaka sala mbi ya kuluta kiteso na bankinsi ya nene ya ba­mi­mpani.\f*
\v 4 Ntangu yai, bo ke yituka sambu beno ke vukana na bo diaka ve na mansoni na bo ya kuluta kiteso. Yo yina, bo ke finga beno.
\v 5 Kansi Nzambi ta tungulula mambi na bo, sambu yandi me yilama na kusambisa bantu : bayina ke zinga ti bayina me fwaka.
\v 6 Yo yina, Nsangu ya mbote kumaka na bayina me fwaka, na mpila nde kana bo belaka na meso ya bantu na luzingu na bo ya kimuntu, Nzambi kuzingisa bo na Mpeve na yandi.
\s1 Beno sadisana beno na beno. Beno sadila makabu ya Nzambi
\p
\v 7 Nsuka ya bima yonso me finama. Yo yina, beno songa malungalala na luzingu na beno. Beno lawuka malafu ve, sambu na kusamba mbote.
\v 8 Mambu ya ntete, beno vanda na lutondo ya ngolo beno na beno, sambu \qt Lutondo ke vwa­nzaka masumu mingi.\qt* \f + \ft Ndinga yai me katuka na Bng 10,12 (ya Yak 5,20 ke yibukisa beto mpi). Mbala yankaka yo ke songidila nde Kristu ta wila mawa na bayina kuwilaka nkweno mawa (bonso na Mateo 5,7). Mbala yankaka yo zola kusongidila nde : lutondo ke nata mambu yonso (na mutindu ya 1 Kor 13,7).\f*
\v 9 Beno kubokuta ve na kuyambana beno na beno na banzo na beno.
\v 10 Na kiteso ya makabu ya Nzambi kabilaka konso muntu, yandi kusadila yo na kusadisa bantu yankaka, bonso ntwadisi ya mbote ya ke bumbaka ve sambu na yandi mosi makabu ya Nzambi ya kele ya mutindu na mutindu.
\v 11 Yina ke zaba kutuba, yandi kutuba ndinga ya ke basika na Nzambi. Yina ke zaba kusadisa, yandi kusala yo ti kikesa ya Nzambi ke pesa yandi. Mpila yina, na mambu yonso, nkembo ta kwenda na Nzambi na nzila ya Yezu Kristu. Na yandi lukumu ti kimfumu na bamvula yonso. Amen.
\s1 Dilongi ya iya
\s1 Beno bantu ya bo ke bangika sambu beno kele ya Kristu
\s1 Beno mona kiese na kati ya mampasi, kana bo kubangika beno sambu beno kele ya Kristu
\p
\v 12 Bampangi ya kutondama, beno kuyituka ve kana mampasi ke ziunga beno bonso tiya ya ngolo sambu na kumeka beno. Beno kumona yo ve bonso mambu ya bo lenda ve kubakisa.\f + \ft Ngindu ke kwendila ti 1,7 : mambu ya wolo ya bo ke yobisa na tiya. Mambu ya v.13 ke kwendila ti Mt 5,11-12. Kiese na bayina ya bantu ya mbi ke bangika. Tala mpi Bis 5,41. Mambu ya v.14 ke kwe­ndila ti Iza 11,2 (Mpeve ya Nzambi ta vanda na zulu ya Mesia, mpila mosi Ngulusi). Ye Kristu (Mesia) silaka na kupesa Mpe­ve na yandi na bayina ta va­nda batemwe na yandi : Mt 10,20.\f*
\v 13 Kansi na kiteso ya beno ke vukana na mampasi ya Kristu, beno yangalala, na mpila nde beno kumona mpi kiese ye beno yangalala ntangu yandi ta monika na nkembo na yandi.
\v 14 Kana bo ke finga beno sambu na zina ya Kristu, kiese na beno, sambu Mpeve ya Nzambi, Mpeve ya nkembo na yandi, kele ti beno.
\v 15 Kana bo ke pesa mpasi na muntu mosi na kati na beno, yo kuvanda ve sambu yandi me fwa bantu, to sambu yandi me yiba, to sambu yandi me sala nku, to sambu yandi me kota nkweno nsoki.
\v 16 Kansi kana kisina na yo kele kimukristu na yandi, yo kupesa yandi nsoni ve, kansi bika yandi kukembila Nzambi, sambu na zina ya Kristu ya yandi me baka.
\v 17 Mpamba ve : yo yai ntangu ya Nzambi ke sambisa bantu. Yandi ke yantika ti bantu ya nzo na yandi. Ibuna kana yandi kuyantika ti beto, nki mutindu yo ta vanda na nsuka ti bayina ke buya na kukwikila Nsangu ya mbote ya ke basika na Nzambi ?
\v 18 Mikanda ya santu ke tuba nde : \qt Kana muntu ya masonga ke mona mpasi na kuguluka, nki mutindu yo ta vanda sambu na yina ke buya Nzambi, yina ke zinga na masumu ?\qt*\f + \ft Ndinga yai ke katuka na Bng 11,31 mutindu bo balulaka yo na kigrek (LXX).\f*
\v 19 Yo yina, bayina ke mona mpasi sambu bo ke landaka luzolo ya Nzambi, bo kufula na kusala mambote ye bo kutula ntima na bo na Nzambi ya me ganga bo, yina ke kwamina na mambu ya yandi silaka.
\c 5
\s1 Dilongi ya tanu
\s1 Bakristu ke kinga Ngisa ya Mfumu na bo. Petelo ke pesa bo bandongisila ya nsuka
\s1 Sambu na bambuta ya Dibundu
\p
\v 1 Mono ke tuba ntangu yai na bayina na kati na beno ya kele bambuta ya Dibundu. Mono mbuta ya Dibundu kumosi ti bo, mono temwe ya mampasi ya Kristu, mono muntu ya ta vukana na nkembo ya ke kwisa kumonika, mono ke bondila bo nde :
\v 2 Beno langidila mbote mameme ya Nzambi, bantu ya yandi tulaka na maboko na beno. Beno sala yo ve na kingolo-ngolo, kansi na luzolo ya mbote, mutindu Nzambi ke zola. Beno sala yo ve na nzala ya mbongo, kansi na ntima ya kusadila.
\v 3 Beno sala yo ve bonso bamfumu ke niokulaka bantu ya Nzambi tulaka na maboko na bo, kansi beno vanda mbandu ya mameme na beno ta landa.
\v 4 Ibuna ntangu Ngungudi ya kele Mfumu ya bangungudi yonso ta monika, yandi ta pesa beno mpu ya lunungu, ya ke bebaka ve, nde Lukumu.\f + \ft Tuka v.1 nsoniki ke tuba na Bambuta (na kigrek prestubetoi), bayina ke twadisa kimvuka ya bakristu (tala 1 Tim 5,17 ye Tt 1,5-9). Petelo ya vandaka temwe ya mampasi ya Yezu, ntangu yai yandi ke mona mpasi (na maboko ya bambeni ya lukwikilu) sambu na kuvukana ti Kristu ya yandi ke zabisa. - Na v.2, bo ke yibukisa beto ndinga ya Yezu na Petelo : « langidila mameme na mono » (Yo 21,15-17) ; tala mpi Bis 20,28. - Na v.3 bo ke yibukisa beto Yoz 13–22 : ntoto ya mvimba ya lusilu kabanaka na kati ya makanda ya Israël. Bo bulaka zeke ; konso kikanda bakaka ndambu na yandi. Mambu yina ke bumbana awa na nsi ya bangogo « bayina ya Nzambi tulaka na maboko ya beno bambuta. » Ye awa, ata Nzambi ke pesa bantu na yandi na maboko ya « bambuta », kansi yandi mosi kele Mfumu na bo. - Na v.3b bo ke yibukisa beto nde ntumwa to Mbuta fwete kupesa mbandu ya mbote, mutindu Polo kusalaka ; tala 1 Kor 4,16 ye 11,1 ye Flp 3,17. - Na v.4 bo ke yibukisa beto nde Yezu kele Ngungudi ; tala Yo 10 ye Lk 15 ye awa 1 P 2,25.\f*
\s1 Sambu na baleke
\p
\v 5 Kiteso mosi, beno baleke, beno lemfuka na bambuta.
\s1 Sambu na bakristu yonso : Mbatalala-mbundu ye lukwikilu ya ngolo
\p
\v 5 Beno yonso, beno kudifiotuna na meso ya bampangi, sambu na kusadisana beno na beno. Mpamba ve : \qt Nzambi ke nwanisa bantu ya lulendo. Kansi bayina ke kudifiotuna, yandi ke pesa bo mambote ya dienga na yandi.\f + \ft Ndinga yai (awa bonso na Yak 4,6) ke katuka na Bng 3,34 mutindu bo balulaka yo na kigrek (LXX). Na ndambu yai yonso (vv.5b-9) Petelo ke yibukisa bangindu ya ke monika na bankembila kumosi ti malongi ya Mfumu Yezu. - Na v.6 « kutombula bayina me kudikulumusa », yo ke kwendila ti Yak 4,6-7 ti Lk 1,52 ye 14,11 ye 18,14. — Na v.6b « ntangu ya Nzambi me tula » kele ntangu ya yandi ta sambisa bantu yonso.\f*\qt*
\v 6 Yo yina, beno kukikulumusa mpi na nsi ya diboko ya ngolo ya Nzambi, sambu yandi kutombula beno na ntangu ya yandi me tula.
\v 7 Mambu yonso ya ke yangisa beno, beno bikisa kilo na yo na Nzambi, sambu yandi ke tanina beno.
\v 8 Beno kangula meso bonso bantu ya me lauka malafu ve, bantu ya ke nwanisa mpongi. Mbeni na beno Satana ke zieta penepene na beno, \qt bonso ntambu ke ngana.\f + \ft Ndinga yai ke basika na Nkb 21 (h 22,14). Yo ke kwendila ti 2 Tim 4,17 ye Yak 4,7 (na nima ya kuyibukisa na 4,6 Bng 3,34). - Nkb 21 (h 22) kele yina ya bakristu kuyibukaka na ntwala ya ba­nkembila yankaka yonso na kuta­ngulula mampasi ya Yezu.\f*\qt* Yandi ke sosa muntu ya kumina.
\v 9 Beno nwana na yandi, bantu ya kusiama na lukwikilu. Beno ke zaba nde bampangi na beno bakristu ke mona mpasi mutindu mosi na ntoto mvimba.
\v 10 Mampasi na beno kele sambu na ntangu fioti. Ibuna Nzambi ya ke pesa makabu ya dienga na yandi, yina me binga beno sambu na kukabila beno, na Kristu, lukumu na yandi ya kukonda nsuka, yandi ta vutula beno na ngemba. Yandi ta siamisa beno, yandi ta pesa beno ngolo na yandi. Yandi ta kitula beno bonso matadi ya bo lenda ve kunikisa.
\v 11 Na yandi kimfumu na bamvula yonso. Amen.
\s1 Na kumanisa
\p
\v 12 Silvain me sadisa mono na kusonikila beno mukanda yai. Yandi kele mpangi ya mono lunga kutula ntima. Mono me sonika mingi ve, kaka sambu na kupesa beno kikesa ye na kuvanda temwe nde : beno me kangama na mambote ya dienga ya kieleka ya Nzambi.\f + \ft Mambu ya Silvain, beto me tuba yo na Mayita. Verset 12 ke tu­ba na nkufi mambu yonso ya Petelo tendulaka na mukanda : yandi ke zo­la kusiamisa lukwikilu ya ba­kristu, kulonga bo sambu bo kuka­nga ntima na kati ya mampasi, ti ngi­ndu nde Nzambi ta konda ve na kulungisa yina ya yandi silaka.\f*
\s1 Mbote ya nsuka
\p
\v 13 Na mbanza yai ya kele bonso Babylone, kimvuka ya bakristu ya Nzambi me sola kumosi ti beno, bo ke pesa beno mbote. Marko, mwana na mono, ke tindila beno mpi mbote.
\v 14 Beno pesana mbote beno na beno, na mutindu ya bo ke songanaka lutondo na kati ya bampangi. Ngemba kuvanda ti beno yonso, bantu ya Kristu.\f + \ft Mambu ya Babylone (v.13a), beto me tendula yo na Ma­yita. Na mambu ya ke tadila Marko (na v.13b), bambuta tubaka nde yandi sonikaka Nsangu ya mbote, na mutindu ya Petelo ku­te­ndulaka luzingu, malongi ti ma­mpasi ya Yezu. Mama ya Marko ke monika na Bis 12,12. Na ntwala ya kuzi­nga ti Petelo, Marko tambulaka ntete ti Polo ti Barnabé (Bis 12,25 ye 13,13), na nima ti Barnabé mpamba (Bis 15,37-39). - Mambu ya « ku­pesana mbote na mutindu ya ba­mpangi » ke tadila yina ya bo ke bingaka na fr. un baiser fraternel. Bo vandaka kupesana yo na nta­ngu ya bo vandaka kukembila Nza­mbi ya Ngwakana ya mpa na bimvuka ya bakristu (tala Rom 16,16 ye 1 Kor 16,20).\f*
